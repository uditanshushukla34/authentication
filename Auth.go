package authentication

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"strings"
)


var pad = []byte("=")
func AKValidate(auth_Key string) bool {
	validated := false
	if auth_Key != "" {
		temp := strings.Split(auth_Key,".")
		if len(temp) != 3{
			return validated
		}
		head:=temp[0]
		payload:=temp[1]
		sign:=temp[2]

		decodedHead,err:=Base64Decode([]byte(head))
		if err!=nil{
			fmt.Println(err.Error())
			return validated
		}
		fmt.Println(decodedHead)
		fmt.Println(string(decodedHead))

		decodedPayload,err:=Base64Decode([]byte(payload))
		if err!=nil{
			fmt.Println(err.Error())
			return validated
		}
		decodedSign,err:=Base64Decode([]byte(sign))
		if err!=nil{
			fmt.Println(err.Error())
			return validated
		}
		fmt.Println(string(decodedPayload))
		fmt.Println(string(decodedSign))

		data := head + "." +payload
		fmt.Println(data)
		validated:=true
		return validated
	} else {
		return validated
	}
}

func Base64Decode(src []byte) ([]byte, error) {
	if n := len(src) % 4; n > 0 {
		src = append(src, bytes.Repeat(pad, 4-n)...)
	}
	buf := make([]byte, base64.URLEncoding.DecodedLen(len(src)))
	n, err := base64.URLEncoding.Decode(buf, src)
	return buf[:n], err
}